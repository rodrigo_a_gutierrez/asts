import Vue from 'vue'
import Router from 'vue-router'
import List from '@/components/list'
import Details from '@/components/details'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'list',
      component: List
    },
    {
      path: '/details',
      name: 'details',
      component: Details
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: Details
    }
  ]
})
